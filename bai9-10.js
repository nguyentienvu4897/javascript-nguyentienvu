class Product {
    constructor(id, name, categoryId, saleDate, quality, isDelete)
    {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.quality = quality;
        this.isDelete = isDelete;
    }
}

var products = [];
function listProduct(){
    return products = [
        new Product(1, 'samsumg a9', 2, "05-04-2022", 10, false),
        new Product(2, 'samsumg a8', 2, "05-04-2022", 10, false),
        new Product(3, 'samsumg a7', 2, "05-04-2022", 10, false),
        new Product(4, 'samsumg a6', 2, "05-04-2022", 10, false)
    ]
}
console.log(listProduct());