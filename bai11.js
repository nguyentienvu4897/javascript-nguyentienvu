class Product {
    constructor(id, name, categoryId, saleDate, quality, isDelete)
    {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.quality = quality;
        this.isDelete = isDelete;
    }

    getId()
    {
        return this.id;
    }
}

var products = [];
function listProduct()
{
    return products = [
        new Product(1, 'samsumg a9', 2, "05-04-2022", 10, false),
        new Product(2, 'samsumg a8', 2, "05-04-2022", 10, false),
        new Product(3, 'samsumg a7', 2, "05-04-2022", 10, false),
        new Product(4, 'samsumg a7', 2, "05-04-2022", 10, false),
        new Product(5, 'samsumg a7', 2, "05-04-2022", 10, false),
        new Product(6, 'samsumg a7', 2, "05-04-2022", 10, false),
        new Product(7, 'samsumg a6', 2, "05-04-2022", 10, false)
    ]
}

//Sử dung Es6
function filterProductById(listProduct, productId)
{
    return listProduct.find((product)=> product.id == productId);
}
console.log(filterProductById(listProduct(), 3));


//Sử dụng hàm for
function filterProductById(listProduct, productId)
{
    for (let index = 0; index < listProduct.length; index++) {
        if (listProduct[index].getId() == productId) {
            return listProduct[index];
        }
    }
}

console.log(filterProductById(listProduct(), 6));