class Product {
    constructor(id, name, categoryId, saleDate, quality, isDelete)
    {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.quality = quality;
        this.isDelete = isDelete;
    }
}

var products = [];
function listProduct(){
    return products = [
        new Product(1, 'samsumg a9', 2, "05-04-2022", 10, false),
        new Product(2, 'samsumg a8', 2, "05-04-2022", 10, true),
        new Product(3, 'samsumg a7', 0, "05-04-2022", 0, false),
        new Product(4, 'samsumg a6', 2, "05-04-2022", 10, false)
    ]
}
//sử dụng ES6
function filterProductQuality(listProduct)
{
    return listProduct.filter((product)=> product.quality > 0 && product.isDelete == false);
}
console.log(filterProductQuality(listProduct()));

//Sử dụng hàm for
let products = '{"products":[' +
'{"id":"1","name":"samsung a9","categoryId":"2","saleDate":"05-04-2022","quality":"10","isDelete":"false" },' +
'{"id":"2","name":"samsung a8","categoryId":"2","saleDate":"05-04-2022","quality":"10","isDelete":"true" },' +
'{"id":"3","name":"samsung a7","categoryId":"2","saleDate":"05-04-2022","quality":"10","isDelete":"false" },' +
'{"id":"4","name":"samsung a6","categoryId":"2","saleDate":"05-04-2022","quality":"10","isDelete":"false" }]}';
const listProduct = JSON.parse(products);
const arrListProduct = listProduct.products;
function filterProductQuality(listProduct)
{
    let productByQuality = [];
    for (let index = 0; index < listProduct.length; index++) {
        if (listProduct[index]['quality'] > 0 && listProduct[index]['isDelete'] == 'false' ) {
            productByQuality.push(listProduct[index]);
        }
    }
    return productByQuality;
}

console.log(filterProductQuality(arrListProduct));