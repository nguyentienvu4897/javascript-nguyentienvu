class Product {
    constructor(id, name, categoryId, saleDate, quality, isDelete)
    {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.quality = quality;
        this.isDelete = isDelete;
    }
}

var products = [];
function listProduct(){
    return products = [
        new Product(1, 'samsumg a9', 2, "05-05-2022", 10, false),
        new Product(2, 'samsumg a8', 2, "02-02-2022", 10, false),
        new Product(3, 'samsumg a7', 2, "12-05-2022", 10, false),
        new Product(4, 'samsumg a6', 2, "01-02-2022", 10, false)
    ]
}

//Sử dụng ES6
function filterProductBySaleDate(listProduct)
{
    return listProduct.filter((product)=> Date.parse(product.saleDate) > Date.now() && product.isDelete == false);
}
console.log(filterProductBySaleDate(listProduct()));


//Sử dụng vòng lặp for
let products = '{"products":[' +
'{"id":"1","name":"samsung a9","categoryId":"2","saleDate":"02-04-2022","quality":"10","isDelete":"false" },' +
'{"id":"2","name":"samsung a8","categoryId":"2","saleDate":"02-04-2022","quality":"10","isDelete":"true" },' +
'{"id":"3","name":"samsung a7","categoryId":"2","saleDate":"05-05-2022","quality":"10","isDelete":"false" },' +
'{"id":"4","name":"samsung a6","categoryId":"2","saleDate":"01-04-2022","quality":"10","isDelete":"false" }]}';
const listProduct = JSON.parse(products);
const arrListProduct = listProduct.products;
function filterProductBySaleDate(listProduct)
{
    let arrProducts = [];
    for (let index = 0; index < listProduct.length; index++) {
        if (listProduct[index]['isDelete'] == 'false' && Date.parse(listProduct[index]['saleDate']) > Date.now()) {
            arrProducts.push(listProduct[index]);
        }
    }
    return arrProducts;
}
console.log(filterProductBySaleDate(arrListProduct));
