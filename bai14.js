class Product {
    constructor(id, name, categoryId, saleDate, quality, isDelete)
    {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.quality = quality;
        this.isDelete = isDelete;
    }
}

var products = [];
function listProduct(){
    return products = [
        new Product(1, 'samsumg a9', 2, "05-05-2022", 100, false),
        new Product(2, 'samsumg a8', 2, "02-02-2022", 10, false),
        new Product(3, 'samsumg a7', 2, "12-05-2022", 10, true),
        new Product(4, 'samsumg a6', 2, "01-02-2022", 10, false)
    ]
}

//Sử dụng ES6
function totalProduct(listProduct)
{
    const filterProductByIsDelete = listProduct.filter((product)=>product.isDelete == false);
    console.log(filterProductByIsDelete);
    return filterProductByIsDelete.reduce((previousValue, currentValue) => previousValue + currentValue.quality, 0);
}
console.log(totalProduct(listProduct()));

//Sử dụng vòng lặp for
function listProduct(){
    return products = [
        {"id":1, "name":"Samsung a9", "categoryId":2,"saleDate":"5/4/2022","quality":2,"isDelete":false},
        {"id":2, "name":"Samsung a8", "categoryId":2,"saleDate":"5/4/2022","quality":2,"isDelete":false},
        {"id":3, "name":"Iphone X", "categoryId":2,"saleDate":"5/4/2022","quality":2,"isDelete":false},
        {"id":4, "name":"Samsung a7", "categoryId":2,"saleDate":"5/4/2022","quality":2,"isDelete":false},
        {"id":5, "name":"Iphone 11", "categoryId":2,"saleDate":"5/4/2022","quality":2,"isDelete":false},
    ]
}
function totalProduct(listProduct)
{
    let total = 0;
    for (let index = 0; index < listProduct.length; index++) {
        total += (listProduct[index].quality);
    }
    return total;
}
console.log(totalProduct(listProduct()));
