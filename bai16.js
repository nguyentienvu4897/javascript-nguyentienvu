class Product {
    constructor(id, name, categoryId, saleDate, quality, isDelete)
    {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.quality = quality;
        this.isDelete = isDelete;
    }
}

var products = [];
function listProduct(){
    return products = [
        new Product(1, 'samsumg a9', 2, "05-04-2022", 10, false),
        new Product(2, 'samsumg a8', 2, "05-04-2022", 10, false),
        new Product(3, 'samsumg a7', 2, "05-04-2022", 10, false),
        new Product(4, 'samsumg a6', 2, "05-04-2022", 0, false)
    ]
}

//Sử dụng ES6
function filterProductBySaleDate(listProduct)
{
    let filterProducts = listProduct.filter((product)=> product.quality > 0 && Date.parse(product.saleDate) > Date.now());
    filterProducts = filterProducts.map((product)=> [product.id ,product.name]);
    return filterProducts;
}
console.log(filterProductBySaleDate(listProduct()));

//Sử dung vòng lặp for
function listProduct(){
    return products = [
        {"id":1, "name":"Samsung a9", "categoryId":2,"saleDate":"5/4/2022","quality":2,"isDelete":false},
        {"id":2, "name":"Samsung a8", "categoryId":2,"saleDate":"5/4/2022","quality":2,"isDelete":false},
        {"id":3, "name":"Iphone X", "categoryId":2,"saleDate":"5/4/2022","quality":2,"isDelete":false},
        {"id":4, "name":"Samsung a7", "categoryId":2,"saleDate":"2/2/2022","quality":2,"isDelete":true},
        {"id":5, "name":"Iphone 11", "categoryId":2,"saleDate":"4/4/2022","quality":2,"isDelete":false},
    ]
}

function filterProductBySaleDate(listProduct)
{
    let arrProduct = [];
    for (let index = 0; index < listProduct.length; index++) {
        if(listProduct[index].quality > 0 && Date.parse(listProduct[index].saleDate) > Date.now())
        {
            arrProduct.push([listProduct[index].id, listProduct[index].name ]);
        }
    }
    return arrProduct;
}

console.log(filterProductBySaleDate(listProduct()));