class Product {
    constructor(id, name, categoryId, saleDate, quality, isDelete)
    {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.quality = quality;
        this.isDelete = isDelete;
    }
}

var products = [];
function listProduct(){
    return products = [
        new Product(1, 'samsumg a9', 2, "05-04-2022", 10, false),
        new Product(2, 'samsumg a8', 2, "05-04-2022", 10, false),
        new Product(3, 'samsumg a7', 2, "05-04-2022", 10, false),
        new Product(4, 'samsumg a6', 2, "05-04-2022", 10, false)
    ]
}

//Sử dụng ES6
function isHaveProductInCategory(listProduct, categoryId)
{
    let filterProductByCategory = listProduct.filter((product)=> product.categoryId == categoryId)
    if (filterProductByCategory == '') {
        return false;
    }else{
        return true;
    }
}
console.log(isHaveProductInCategory(listProduct(), 2));

//Sử dụng vòng lặp for
function listProduct(){
    return products = [
        {"id":1, "name":"Samsung a9", "categoryId":3,"saleDate":"5/4/2022","quality":2,"isDelete":false},
        {"id":2, "name":"Samsung a8", "categoryId":2,"saleDate":"5/4/2022","quality":2,"isDelete":false},
        {"id":3, "name":"Iphone X", "categoryId":3,"saleDate":"5/4/2022","quality":2,"isDelete":false},
        {"id":4, "name":"Samsung a7", "categoryId":2,"saleDate":"5/4/2022","quality":2,"isDelete":false},
        {"id":5, "name":"Iphone 11", "categoryId":4,"saleDate":"5/4/2022","quality":2,"isDelete":false},
    ]
}
console.log(listProduct());
function isHaveProductInCategory(listProduct, categoryId)
{
    for (let index = 0; index < listProduct.length; index++) {
        if (listProduct[index].categoryId == categoryId) {
            return true;
        }
    }
    return false;
}
console.log(isHaveProductInCategory(listProduct(), 2));